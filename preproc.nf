#!/usr/bin/env nextflow

include { get_fastqs } from '../utilities/utilities.nf'

include { fastp } from '../fastp/fastp.nf'
include { trim_galore } from '../trim_galore/trim_galore.nf'
include { trimmomatic } from '../trimmomatic/trimmomatic.nf'

workflow manifest_to_raw_fqs {
// require:
//   MANIFEST
  take:
    manifest
  main:
    get_fastqs(
      manifest.map{ [it[0], it[1], it[2], it[3]] }, //tuple val(pat_name), val(prefix), val(dataset), val(run)
      params.fq_dir)
  emit:
    fqs = get_fastqs.out.fastqs
}


workflow manifest_to_procd_fqs {
// require:
//   MANIFEST
//   params.preproc$manifest_to_procd_fqs$fq_trim_tool
//   params.preproc$manifest_to_procd_fqs$fq_trim_tool_parameters
  take:
    manifest

  main:
    manifest_to_raw_fqs(
      manifest)
    raw_fqs_to_procd_fqs(
      manifest_to_raw_fqs.out.fqs,
      fq_trim_tool,
      fq_trim_tool_parameters)
  emit:
    procd_fqs = raw_fqs_to_procd_fqs.out.procd_fqs
}


workflow raw_fqs_to_procd_fqs {
// require:
//   FQS
//   params.preproc$raw_fqs_to_procd_fqs$fq_trim_tool
//   params.preproc$raw_fqs_to_procd_fqs$fq_trim_tool_parameters
  take:
    fqs
    fq_trim_tool
    fq_trim_tool_parameters
  main:
    // Trim Galore
    fq_trim_tool_parameters = Eval.me(fq_trim_tool_parameters)
    if ( fq_trim_tool =~ /trim_galore/ ) {
      trim_galore_parameters = fq_trim_tool_parameters['trim_galore'] ? fq_trim_tool_parameters['trim_galore'] : ''
      trim_galore(
        fqs,
        trim_galore_parameters)
      trim_galore.out.procd_fqs.set{ procd_fqs }
    }
    // Trimmomatic
    if ( fq_trim_tool =~ /trimmomatic/ ) {
      trimmomatic_parameters = fq_trim_tool_parameters['trimmomatic'] ? fq_trim_tool_parameters['trimmomatic'] : ''
      trimmomatic(
        fqs,
        trimmomatic_parameters)
      trimmomatic.out.procd_fqs.set{ procd_fqs }
    }
    // fastp
    if ( fq_trim_tool =~ /fastp/ ) {
      fastp_parameters = fq_trim_tool_parameters['fastp'] ? fq_trim_tool_parameters['fastp'] : ''
      fastp(
        fqs,
        fastp_parameters)
      fastp.out.procd_fqs.set{ procd_fqs }
    }
    // Skip
    if ( fq_trim_tool == '' ) {
      println("No trimming tool selected. Passing FASTQs through.")
      fqs.set{ procd_fqs }
    }
  emit:
    procd_fqs
}
